\documentclass{beamer}
\usetheme{Warsaw}
\setbeamertemplate{headline}{}
\graphicspath{ {./images/} }
\usepackage{amsmath}

\title[Binding Residue Prediction]{Protein-Ligand Binding Residue Prediction using CNN
}
\author[Abdus Salam Khazi]
{
                \href{https://doi.org/10.1186/s12859-019-2672-1}
                        {Research Paper} \cite{ResearchPaper}
}

\institute{\textbf{BioInformatics Seminar}\\
                Abdus Salam Khazi\\
                Albert-Ludwigs-Universität Freiburg}
\date{\today}
\logo{\includegraphics[width=.2\textwidth]{Logo}}

\hypersetup{
    colorlinks=true,
    linkcolor=blue,
    filecolor=magenta,      
    urlcolor=cyan,
}

\begin{document}

\begin{frame}
\titlepage
\end{frame}

\begin{frame}{Table of Contents}
\tableofcontents
\end{frame}

\section{Biological Background}

\begin{frame}[t]{Biological Background}
What are proteins? 
\begin{itemize}
\item They are complex molecules that are work-horses of a living organism.
They are literally machines.
\cite{TedXTalk}

\item Proteins are essential for almost every function of a living organism's body.
E.g: Movement of muscles, Transportation of oxygen, Digestion, Immune system etc...

\item Below are 2 proteins - Hemoglobin(Left) and Motor Proteins(Right).
Notice their shapes are different. \cite{HemoglobinImage} \cite{MotorProteinImage}

\includegraphics[scale=0.25]{Hemoglobin} 
\includegraphics[scale=0.25]{MotorProtein}
\end{itemize}
\end{frame}


\begin{frame}[t]{Biological Background}
What are Ligands?

\begin{itemize}
\item They are molecules that bind to particular proteins, called receptor proteins.
They signal the proteins to start a process.
\item Example - Glucagon is a ligand created by pancreas. This binds to Glucagon receptor protein which signals it to control the blood sugar level.
\end{itemize}


Why study protein Ligand Interaction?


\begin{itemize}
\item It will help us understand biological processes better.
\item In the above example we can develop better treatment for type-2 diabetes by understanding the working of Glucagon and its receptor protein.
\end{itemize}
\end{frame}

\section{Problem Description}

\begin{frame}[t]{What is our problem?}
\begin{itemize}
\item In this paper the authors are trying to estimate the function
$f:\mathbb \alpha^m \to \{Binding, NonBinding\}^m$
where $m$ is the length of any ligand binding protein and $\alpha \in $
\{Set of amino acids possible in a protein\}.

\item This function is learnable because the binding sites are directly influenced by the tertiary protein structure which in turn depends on its primary structure.

\item Notice that $m$ is variable here!!!

\item Previous attempts to solve this problem have used
SVM, Deep Neural networks, Bayes classifier, 3D Template matching (albeit with high computational costs).
\end{itemize}
\end{frame}

\section{Proposed Solutions}

\begin{frame}[t]{Proposed Solutions}

\begin{itemize}
\item Neural Machine Translation (NMT).
Binding-Site prediction can be formulated as a specific case of Neural machine Translation (NMT).
Hence, any solution of NMT can be used for our problem.
\item Our problem can also be formulated as a Recurrent neural network (RNN) problem as length of the proteins is variable.
\item There are disadvantages e.g. Computational time (among others)

\item Solution - Temporal Convolution Neural Networks.
They can be parallelized, are 10x faster and give state of the art results.

\end{itemize}
\end{frame}

\section{CNN As Solution}

\begin{frame}[t]{CNN as a solution}
\begin{itemize}
\item Each amino-acid in a protein is mapped to $\mathbb{R}^{30}$. Thus a protein is mapped to a feature space $\mathbb{R}^{m \times 30}$

\item $\mathbb{R}^{30}$ are manually engineered features given by Position-Specific Score Matrix (PSSM),  Relative solvent accessibility etc... \cite{ResearchPaper}
\includegraphics[scale=0.4]{FeatureMap}
\end{itemize}
\end{frame}

\section{CNN Architecture and Design}

\begin{frame}[t]{CNNs as a solution - Architecture}
\textbf{Effective Context Scope / Input Field} \cite{FCNNDrawingTool}

\includegraphics[scale=0.35]{kernel}
\includegraphics[scale=0.25]{EmptyNeuralNetwork}\\
Consider the above kernel with width $3$ (left) and a neural network (right). 
Let $\mathbf{I}$, $\mathbf{H}$ and $\mathbf{O}$ denote the input, hidden and output vectors/layers respectively. 
Let $\mathbf{I}_i$, denote a single node in the \\
input vector.
Here $\mathbf{I}_0$ is the top element in the input vector\\
and in the image.
\end{frame}

\begin{frame}[t]{CNNs as a solution - Architecture}
\textbf{Effective Context Scope / Input Field}

When we apply Kernel to both $\mathbf{I}$ and $\mathbf{H}$ with stride 1,\\

$Context(\mathbf{O}_0) = \{H_0, H_1, H_2\}$\\
$Input Field(\mathbf{H}_0) = \{I_0, I_1, I_2\}$\\
$Input Field(\mathbf{H}_1) = \{I_1, I_2, I_3\}$\\
$Input Field(\mathbf{H}_2) = \{I_2, I_3, I_4\}$\\
$Input Field(\mathbf{O}_0) = Input Field(\mathbf{H}_0) \cup 
                                                   Input Field(\mathbf{H}_1) \cup 
                                                   Input Field(\mathbf{H}_2)$\\
$Input Field(\mathbf{O}_0) = \{I_0, I_1, I_2, I_3, I_4\}$\\
$|Input Field(\mathbf{O}_0)| = 5 = 1 + 2(3 -1) = 1 + n(k-1)$


\begin{itemize}
\item Hence a single convolutional node in $n^{th}$ convolutional layer (depth) with kernel width $k$ and stride 1 has an input field $1+ n(k-1)$. 

\item The authors propose an increase in $n$ i.e the depth of CNN instead of $k$ to prevent huge growth in optimization parameters.
\end{itemize}
\end{frame}

\subsection{Basic Design}

\begin{frame}[t]{Basic Design - Standard DeepCSeq-Site}
The following diagram shows the design of the model proposed in the paper. \cite{ResearchPaper} \\

\includegraphics[scale=0.4]{StandardDesign}

\end{frame}

\begin{frame}[t]{Basic Design - Standard DeepCSeq-Site}
Here we consider batch size $B = 1$ for simplicity.
Preprocessed protein $\textbf{P} \in \mathbb{R}^{m \times d}$ is input to the DeepCSeq network.
Here $d = 30$ and $m$ is the length of the protein in the batch.
\begin{itemize}

\item Let Kernel $\mathbf{K} \in \mathbb{R}^{k \times d}$ be the 1-D kernel in our convolutions.

\item We apply same convolutions using these kernels to maintain length $m$.
\ifx false
\item $\mathbf{0}^{\frac{k}{2} \times d}$ is padded at both ends of \textbf{P} to maintain length $m$. This padding is also done at each step throughout the network for the same reason.\fi

\item To capture local co-relation and covalent bonds between adjacent residues,
the following convolution is applied
$$
[\mathbf{K}_1(\textbf{P})\:\: 
\mathbf{K}_2(\textbf{P})\:\:
 \cdots \mathbf{K}_{2c}(\textbf{P})]^T  
\in \mathbb{R}^{m \times 1 \times 2c}
$$

\item $\mathbb{R}^{m \times 1 \times 2c}$ is input to $N$ Basic blocks which form the\\ encoder of the network.
\end{itemize}
\end{frame}

\subsection{Encoder Design}

\begin{frame}[t]{Basic Block - DeepCSeq-Site}
A basic block in the network is defined as
$$
Block : \mathbb{R}^{m \times 1 \times 2c} \to \mathbb{R}^{m \times 1 \times 2c}
$$

A series of $N$ blocks are applied in the following form
$$
Block_N(Block_{N-1}.... Block_2(Block_1(\mathbf{X})))); \mathbf{X} \in \mathbb{R}^{m \times 1 \times 2c}
$$

The following diagram shows the basic architecture of a basic block \cite{ResearchPaper} \\

\includegraphics[scale=0.5]{BasicBlock}

\end{frame}


\begin{frame}[t]{Basic Block - DeepCSeq-Site}
Solutions for gradient issues
\begin{itemize}
\item There is a skip connection from the input of the block to the output.
This solves the vanishing gradient issue. The block function can be defined as
$$
Block(\mathbf{X}) = \mathbf{X} + Convolution( GLU( Normalization(\mathbf{X}) ) )
$$

\item Normalization prevents unexpected gradient due to the skip connection summation.
\end{itemize}
\end{frame}

\subsection{Decoder Design}

\begin{frame}[t]{Decoder - Standard DeepCSeq-Site}
2 Fully connected (convolution) layers are defined for the decoder in the following way 
\begin{itemize}
\item \textbf{Layer 1} - $\mathbf{K}^{1 \times 1 \times 2c}$ kernels are applied to get the first convolution output $\mathbf{X}_{hidden} \in \mathbb{R}^{m \times 1 \times c}$.
$$
\mathbf{X}_{hidden} = [\mathbf{K}_1(\mathbf{X})\:\: \mathbf{K}_2(\mathbf{X}) \cdots  
\mathbf{K}_c(\mathbf{X})]^T;
\mathbf{X} \in \mathbb{R}^{m \times 1 \times 2c};
$$
Here drop-out regularization is used.

\item \textbf{Layer 2} - $\mathbf{K}^{1 \times 1 \times c}$ kernels are applied to get the final prediction $\mathbf{Y}$.
$$
\mathbf{Y} = [\mathbf{K}_1(\mathbf{X}_{hidden})\:\:
\mathbf{K}_2(\mathbf{X}_{hidden})]^T;
\mathbf{Y} \in \mathbb{R}^{m \times 1 \times 2};
$$
$\mathbf{Y}$ is passed through a soft-max layer to get the values\\
in the range $[0,1]$. 
\end{itemize}
\end{frame}

\begin{frame}[t]{Loss function - Standard DeepCSeq-Site}
The loss function used is the negative cross entropy (for 2-way softmax classifier) defined for a single amino-acid $i$ in the sequence as 
$$
J_i(\theta) = - \mathbf{Y}_i \cdot log(\hat{\mathbf{Y}}_i);
\mathbf{Y}_i , \hat{\mathbf{Y}}_i \in \mathbb{R}^2
$$
$$
y_i \in \{0,1\};
\hat{y_i} \in [0,1];
\sum \mathbf{Y}_i = \sum \hat{\mathbf{Y}}_i = 1
$$
The final loss function for a protein-ligand prediction is given by
$$
J(\theta) = \sum_{i}^{m} J_i(\theta) + \gamma * L2Norm(\theta)
$$
After training is complete the classification threshold of softmax is set to a value $< 0.5$ because of non binding skew in data.
\end{frame}

\begin{frame}[t]{Enhanced DeepCSeq-Site}
Here we can see the enhanced model proposed in the paper.
Notice that there is no difference in the encoder.
\cite{ResearchPaper} \\

\includegraphics[scale=0.4]{EnhancedModel}

\end{frame}

\begin{frame}[t]{Decoder - Enhanced DeepCSeq-Site}
The probability of being a binding amino acid depends on wether its neighbours are binding or not. So to facilitiate this,  the following enhanced decoder is proposed by the authors

\begin{itemize}
\item Additional "Context prediction" is given as input to the enhanced decoder.

\item Predictions $\mathbf{P}_{left}$, $\mathbf{P}_{right}$ and the encoder output are all
added to form enhanced encoder's output.

\item \textbf{Teacher forcing at a higher abstraction}.
During training, we can directly use labels as the input to context prediction usage process.
During testing/future-use, the output of the standard decoder is given as the input to the enhanced decoder.

\item Then the decoder used is exactly the same as the standard decoder.
\end{itemize}
\end{frame}

\section{Optimisation}

\begin{frame}[t]{Optimisation - Hyper parameters}
3 main hyper parameters were optimized - number of basic blocks $N$
, kernel width $k$ and the number of channels $c$ using "Student Gradient Decent".
\begin{itemize}
\item As per empirical evidence, $N=10$, $k=5$, $c=256$, $\gamma = 0.2$ and drop out ratio 0.5 gave best results.
\end{itemize}
\includegraphics[scale=0.5]{optimization_N} \cite{ResearchPaper}
\includegraphics[scale=0.5]{optimization_k} \cite{ResearchPaper}
\end{frame}

\begin{frame}[t]{Optimisation - Parameters}

\begin{itemize}
\item The learning rate consists of 3 stages. Each stage has exponential decay.
\item The optimization is done through stochastic gradient descent
\item To prevent data skew, the mini-batch selection should be such that the proportion
of binding to non-binding residues remains roughly the same to that of the whole training data.
\end{itemize}
\end{frame}

\section{Evaluation}

\begin{frame}[t]{Evaluation}
Let $TP$, $TN$, $FP$ and $FN$ be the number of True positive, True
negative, False positive and False negative values of our prediction respectively.
3 metrics are used for evaluation. 
\begin{itemize}
\item Mathew's correlation coefficient,  $MCC$
$$
MCC = \frac{TP * TN - FP * FN}{\sqrt{(TP+FP)(TP+FN)(TN+FP)(TN+FN)}}
$$
\item Precision
$$
precision = \frac{TP}{TP + FP}
$$
\item Recall
$$
recall = \frac{TP}{TP + FN}
$$
\end{itemize}
\end{frame}


\section{Results}

\begin{frame}[t]{Results}
\begin{itemize}
\item The results were compared with 3D structure based models as they are the models
with best results - TM-SI, COF and COA
\item Standard DeepCSeq-Site is better than these by 0.05 in MCC score and 15\% in precision
\item There is bad Recall score because of non-binding bias in the data sets.
The following tables summarize the results. 
\cite{ResearchPaper}
\end{itemize}
\begin{figure}[]
 \centering
\includegraphics[scale=0.33]{ResultComparison}
\includegraphics[scale=0.33]{EnhancedResults}
\end{figure}
\end{frame}

\section{Discussion and Future Work}

\begin{frame}[t]{Discussion and conclusion}
\begin{itemize}
\item Traditional SVM and ANN do not utilize data effectively.  They have no feature learning, 
low generalization and representational capacity.
\item Deep CNN are better models. They  self learn hierarchical features and learn more abstract features at greater depths. Due to availability of massive sequencing data, 
generalization of the deep networks is possible.
\item They have advantage of capturing long distance relationships at higher depths without depending on a particular input field window.
\end{itemize}
\end{frame}

\begin{frame}[t]{Future Work}
\begin{itemize}
\item Usage of attention mechanisms to capture long distance coorelations between
amino-acids.
\item Inclusion of more 3D structure data in the feature map.
\item Usage of Generative adverserial networks to tackle quantitative disparity between
3D structure data and sequence data.
\end{itemize}
\end{frame}

\section{Questions Please !!!}

\begin{frame}[t]{Questions Please !!!}
  \centering \Huge
  \emph{Q \& A}
\end{frame}

\section{References}

\begin{frame}[t]{References}

\begin{thebibliography}{1}

\bibitem{ResearchPaper}
\alert{Yifeng Cui, Qiwen Dong, Daocheng Hong and Xikun Wang}
\newblock {Predicting protein ligand binding residues with deep convolutional neural networks.}

\bibitem{TedXTalk}
\alert{The protein folding problem. 
\href{https://www.youtube.com/watch?v=zm-3kovWpNQ}{TEDx Talk}}

\bibitem{HemoglobinImage}
\alert{\href{https://en.wikipedia.org/wiki/Hemoglobin}{Hemoglobin Wiki}}

\bibitem{MotorProteinImage}
\alert{Qian Wang,Anatoly B Kolomeisky}
\newblock{
\href{https://www.x-mol.com/paper/5733541}{Theoretical Analysis of Run Length Distributions for Coupled Motor Proteins}}

\bibitem{FCNNDrawingTool}
\alert{NN SVG. 
\href{http://alexlenail.me/NN-SVG/index.html}{FCNN Drawing Tool}
}

\bibitem{DNAtoProtein}
\alert{\href{https://www.youtube.com/watch?v=gG7uCskUOrA}{Protein formation Process }}

\end{thebibliography}

\end{frame}

\end{document}
